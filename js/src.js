var Application= PIXI.Application,
    loader= PIXI.loader,
    resources= PIXI.loader.resources,
    Sprite= PIXI.Sprite,
    Graphics= PIXI.Graphics,
    Container= PIXI.Container;
    Text= PIXI.Text;
game={};
game.keyboard={};

var sprite;
game.screen={width:640, height:360};
game.antialias= true;
game.score={};
game.score.text=new Text("score: ");
game.score.point=0;

game.app= new Application({ width: game.screen.width, height: game.screen.height, antialias: game.antialias }) ;
document.body.appendChild(game.app.view);
game.app.renderer.backgroundColor = 0xF1FFE2;

game.keyboard.getValue= function(key){
  switch(key)
  {
      case 'left': return 39;
      case 'right': return 37;

  }
};
game.keyboard.parseInput= function(event)
{
    switch(event.keyCode) {
        case game.keyboard.getValue('left'):
            if(game.paddle.x<game.screen.width-game.paddle.width) {


                game.paddle.x += 4;
            }

            break;
        case game.keyboard.getValue('right'):
            if(game.paddle.x>0)
            {
                game.paddle.x-=4;
            }

            break;
    }


};
{

}



loader
    .add("ball","img/ball.png")
    .load(setup);
game.ball = new Graphics();
game.paddle= new Graphics();
game.rectangle=new Graphics ;
game.removerectangle=new Graphics ;
game.bricks={row: 5,
   column:7,
    width:75,
    height:20,
    padding:10,
    offsetTop:30,
    offsetLeft:30
};


function fillbricks() {
    var bricks=[];
    for (var c = 0; c < game.bricks.column; c++) {
        bricks[c] = [];
        for (var r = 0; r < game.bricks.row; r++) {
            bricks[c][r] = {x: 0, y: 0, status:1};

        }
    }
    return bricks;
}

game.bricks.array= fillbricks();
game.bricks.draw= function()
{


};
 function setup()
{
    game.ball.radius= 15;
    game.ball.beginFill(0x9966FF);
    game.ball.drawCircle(0, 0, game.ball.radius);
    game.ball.endFill();
    game.ball.vx=1;
    game.ball.vy=1;
    game.ball.x = 64;
    game.ball.y = 330;

    game.paddle.lineStyle(4, 0x99CCFF, 1);
    game.paddle.beginFill(0xFF9933);
    game.paddle.drawRoundedRect(0, 0, 125, 16, 10);
    game.paddle.endFill();
    game.paddle.x=48;
    game.paddle.y = 320;
    game.score.text.position.set(10, 1);
    for(var c=0;c<game.bricks.column;c++)
    {
        for(var r=0;r<game.bricks.row;r++)
        {



                var brickx = (c * (game.bricks.width + game.bricks.padding)) + game.bricks.offsetLeft;
                var bricky = (r * (game.bricks.height + game.bricks.padding)) + game.bricks.offsetTop;
                var color = '0x' + Math.floor(Math.random() * 16777215).toString(16);
                console.log(color);
                game.bricks.array[c][r].x = brickx;
                game.bricks.array[c][r].y = bricky;
            if(game.bricks.array[c][r].status==1) {
                game.rectangle.beginFill(color);
                game.rectangle.drawRect(brickx, bricky, game.bricks.width, game.bricks.height);
                game.rectangle.endFill();


            }






        }
    }



    game.state= play;
    game.app.stage.addChild(game.ball);
    game.app.stage.addChild(game.paddle);
    game.app.stage.addChild(game.rectangle);
    game.app.stage.addChild(game.score.text);


    game.app.ticker.add(delta => gameloop(delta));


}
function gameloop(delta)
{

    game.state(delta);
}
function play(delta)
{



    if(game.ball.x+game.ball.vx>game.screen.width-game.ball.radius||game.ball.x+game.ball.vx<game.ball.radius){
        game.ball.vx=-game.ball.vx;
    }
    if(game.ball.y+game.ball.vy<game.ball.radius)
    {
        game.ball.vy=-game.ball.vy;
    }
    else if(game.ball.y+ game.ball.vy>game.screen.height-game.ball.radius)
    {
        if(game.ball.y>game.paddle.y && game.ball.x< game.paddle.x + game.paddle.width )
        {
            game.ball.vy= -game.ball.vy;


        }
        else{
            alert('game over');
            document.location.reload();
        }
    }
    collisionDetection();
    game.ball.x+=game.ball.vx;
    game.ball.y+=game.ball.vy;


}

function collisionDetection()
{
    for(var c=0;c<game.bricks.column;c++)
    {
        for(var r=0;r<game.bricks.row; r++)
        {
            var b= game.bricks.array[c][r];
            if(b.status==1) {


                if (game.ball.x > b.x && game.ball.x < b.x + game.bricks.width && game.ball.y > b.y && game.ball.y < b.y + game.bricks.height) {
                    game.ball.vy = -game.ball.vy;
                  game.bricks.array[c][r].status=0;
                    game.score.point++;
                    game.score.text.text= "score: "+ game.score.point;
                    if(game.score.point==game.bricks.row+game.bricks.count)
                    {
                        alert("You win")
                        document.location.reload();
                    }






                }
            }
        }
    }
}
